//
//  GPS_Filters.swift
//  bgs-ios
//
//  Created by Alexandru Nistor on 13/07/16.
//  Copyright © 2016 ASSIST Software. All rights reserved.
//

import MapKit

class GPS_Filter {
    private var prevLocation : CLLocation!
    private var step : Int = 0
    private var totalDistancesWithErrors : CLLocationDistance = 0
    private let threshold : CLLocationDistance = 6.0
    private let displayTime : Double = 2.0
    
    var averageThreshold : CLLocationDistance = 1.25
    var distance : CLLocationDistance = 0
    var averageDistance : CLLocationDistance = 0
    
    
    func statisticalCorrectionFilter(newLocation : CLLocation) -> (CLLocation, Bool) {
        
        if step == 0 {
            self.prevLocation = newLocation
            self.step += 1
            return (newLocation, true)
        }else if step == 1 {
            self.distance = newLocation.distanceFromLocation(self.prevLocation)
            
            self.totalDistancesWithErrors += self.distance + newLocation.horizontalAccuracy
            self.step += 1
            
            return (newLocation, true)
        } else {
            self.averageDistance = self.totalDistancesWithErrors / CLLocationDistance(self.step - 1)
            
            self.distance = newLocation.distanceFromLocation(self.prevLocation)
            
            if (self.averageDistance - newLocation.horizontalAccuracy) / self.averageDistance > 0.5 {
                self.resetFilter()
                return (newLocation, true)
            }
            
            if self.averageDistance * self.averageThreshold >= self.distance + newLocation.horizontalAccuracy {
                self.totalDistancesWithErrors += self.distance + newLocation.horizontalAccuracy
                self.prevLocation = newLocation
                self.step += 1
                
                return (newLocation, true)
            }
            
            self.totalDistancesWithErrors += newLocation.horizontalAccuracy
            
            return (newLocation, false)
        }
    }
    
    private func resetFilter(){
        self.step = 0
        self.averageDistance = 10
        self.totalDistancesWithErrors = 0
    }
}
