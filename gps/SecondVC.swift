//
//  SecondVC.swift
//  gps
//
//  Created by Alexandru Nistor on 26/07/16.
//  Copyright © 2016 Alexandru Nistor. All rights reserved.
//

import UIKit
import MapKit
import CoreMotion

class SecondVC: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    // MARK: - Public Variables
    private var myPin : MKPointAnnotation!
    
    private var pastDistanceTravelled : CLLocationDistance! = 0
    private var currentDistanceTravelled : CLLocationDistance! = 0
    private var currentHeading : CLLocationDirection! = 0
    
    
    private var startPoint : CLLocation = CLLocation(latitude: 47.640235, longitude: 26.259403)
    private var region : MKCoordinateRegion!
    
    private var locationManager = CLLocationManager()
    private let activityManager = CMMotionActivityManager()
    private let pedoMeter = CMPedometer()
    
    
    private var allLocations : [CLLocation] = []
    
    private var isMoving : Bool = false
    
    private var outputString : String = ""
    
    private var usingGPS : Bool = true
    private var locTimer : NSTimer!
    
    private var currentFloor : Int = 0
    
    // MARK: - IBOutlets
    @IBOutlet weak var mapKitView: MKMapView!
    @IBOutlet weak var floorLabel: UILabel!
    
    // MARK: - Overriden Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        
        self.mapKitView.delegate = self
        
        self.showMyPoint(startPoint)
        
        self.region = MKCoordinateRegionMakeWithDistance(startPoint.coordinate, 50, 50)
        self.mapKitView.setRegion(region, animated: true)
        // Start updateding data
        
        self.checkIfMoving()
        
        self.locationManager.headingFilter = 5
        self.locationManager.startUpdatingHeading()
        
        self.locTimer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(SecondVC.updateLoc), userInfo: nil, repeats: true)
        self.locTimer.fire()
        
        self.pedoMeter.startPedometerUpdatesFromDate(NSDate()) { (data , error) in
            if(error == nil){
                if let d = data {
                    if self.usingGPS {
                        return
                    }
                    self.currentDistanceTravelled = CLLocationDistance(d.distance!)
                    
                    if self.pastDistanceTravelled == 0 {
                        self.pastDistanceTravelled = self.currentDistanceTravelled
                    }
                    
                    if d.floorsDescended != nil && d.floorsAscended != nil {
                        self.currentFloor = d.floorsAscended!.integerValue - d.floorsDescended!.integerValue
                    }
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.floorLabel.text! = "Current Floor: \(self.currentFloor)"
                    })
                    self.calculateNewCoordinte()
                }
            }
        }
    }
    
    // MARK: - IBActions
    
    // stop using PDR and switch to GPS
    @IBAction func stopAction(sender: AnyObject) {
        self.writeAllDataToFile()
        self.usingGPS = true
        self.locTimer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(SecondVC.updateLoc), userInfo: nil, repeats: true)
        self.locTimer.fire()
    }
    
    // Start using PDR and switch off GPS
    @IBAction func startAction(sender: AnyObject) {
        self.usingGPS = false
        self.locTimer.invalidate()
    }
    
    @objc func updateLoc(){
        self.locationManager.startUpdatingLocation()
    }
    
    private func writeAllDataToFile(){
        FileManager.sharedInstance.writeToFile(self.outputString)
        FileManager.sharedInstance.fileIndex += 1
        
        self.addAllPointsOnMap(self.allLocations)
        
        self.allLocations.removeAll()
        self.allLocations.append(self.startPoint)
        self.outputString = ""
    }
    
    private func checkIfMoving(){
        if(CMMotionActivityManager.isActivityAvailable()){
            self.activityManager.startActivityUpdatesToQueue(NSOperationQueue.mainQueue(), withHandler: { (data) -> Void in
                if let d = data {
                    if(d.stationary == true){
                        self.isMoving = false
                    } else if (d.walking == true){
                        self.isMoving = true
                    } else if (d.running == true){
                        self.isMoving = true
                    } else if (d.automotive == true){
                        self.isMoving = false
                    }
                }
            })
        }
    }
    
    private func calculateNewCoordinte(){
        if !self.isMoving || self.allLocations.isEmpty || self.currentHeading == 0{
            return
        }
        
        if self.usingGPS {
            return
        }
        
        // distance in meters
        let dif = (self.currentDistanceTravelled - self.pastDistanceTravelled)
        
        if dif < 4.5 {
            return
        }
        
        let newLoc = PDR.findPointAtDistanceFrom(self.allLocations.last!, initialBearingRadians: PDR.degreesToRadians(self.currentHeading), distanceKilometres: dif / 1000)
        
        self.outputString += "\(newLoc.coordinate.latitude),\(newLoc.coordinate.longitude)   F:\(self.currentFloor)\n"
        
        self.pastDistanceTravelled = self.currentDistanceTravelled
        
        self.allLocations.append(newLoc)
        
        self.showMyPoint(newLoc)
    }
    // MARK: - Location Manager Implementation
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if (newHeading.headingAccuracy < 0) {
            return
        }
        
        if self.usingGPS {
            return
        }
        
        // Use the true heading if it is valid.
        let theHeading = (newHeading.trueHeading > 0) ? newHeading.trueHeading : newHeading.magneticHeading
        
        self.currentHeading = theHeading
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.startPoint = locations.last!
    
        self.currentHeading = self.startPoint.course
        
        self.allLocations.removeAll()
        self.allLocations.append(self.startPoint)
        self.showMyPoint(self.startPoint)
        
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blueColor()
            polylineRenderer.lineWidth = 2
            return polylineRenderer
        }
        
        return MKOverlayRenderer()
    }
    
    func showMyPoint(loc : CLLocation){
        if self.myPin == nil {
            self.myPin = MKPointAnnotation()
        }
        
        self.myPin.coordinate = loc.coordinate
        
        if self.mapKitView.annotations.isEmpty {
            self.mapKitView.addAnnotation(self.myPin)
        }
    }
    
    private func addAllPointsOnMap(array : [CLLocation]){
        var point : [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for p in array {
            point.append(p.coordinate)
        }
        
        let polyLine = MKPolyline(coordinates: &point, count: point.count)
        
        self.mapKitView.addOverlay(polyLine)
    }
}
