//
//  FileManager.swift
//  gps
//
//  Created by Alexandru Nistor on 25/07/16.
//  Copyright © 2016 Alexandru Nistor. All rights reserved.
//

import UIKit
import MapKit

class FileManager {
    static let sharedInstance = FileManager()
    
    var fileIndex : Int = 0
    var locationIndex : Int = 0
    var dummyLocations : [CLLocation] = []

    func writeToFile(data : String){
        // Save data to file
        let fileName = "coord_\(fileIndex)"
        
        let DocumentDirURL = try! NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
        
        let fileURL = DocumentDirURL.URLByAppendingPathComponent(fileName).URLByAppendingPathExtension("txt")
        
        do {
            // Write to the file
            try data.writeToURL(fileURL, atomically: true, encoding: NSUTF8StringEncoding)
        } catch let error as NSError {
            print("Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
        }
    }
    
    func readFromFile() {
        let fileName = "input.csv"
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let path = documentsPath.stringByAppendingPathComponent(fileName)
        
        var readText : String = ""
        
        do {
            try readText = NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding) as String
        }
        catch let error as NSError {
            print("ERROR : reading from file \(fileName) : \(error.localizedDescription)")
            return
        }
        
        let timmed = readText.stringByReplacingOccurrencesOfString("\"", withString: "")
        let split = timmed.componentsSeparatedByString("\n")
        
        for i in 1 ..< split.count {
            if split[i] != "" {
                let lineSplit = split[i].componentsSeparatedByString(",")
                let lat = CLLocationDistance(lineSplit[2])!
                let long = CLLocationDistance(lineSplit[3])!
                let acc = Double(lineSplit[4])!
                let df = NSDateFormatter()
                df.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let finalD = df.dateFromString(lineSplit[lineSplit.count - 2])!
                
                let coord = CLLocationCoordinate2D(latitude: lat, longitude: long)
                
                let newLoc = CLLocation(coordinate: coord, altitude: 0, horizontalAccuracy: acc, verticalAccuracy: 0, timestamp: finalD)
            
                self.dummyLocations.append(newLoc)
            }
        }
    }
}


class PDR {
    class func findPointAtDistanceFrom(startPoint : CLLocation, initialBearingRadians : Double, distanceKilometres : Double) -> CLLocation
    {
        let radiusEarthKilometres : Double = 6371.01
        let distRatio = distanceKilometres / radiusEarthKilometres
        let distRatioSine = sin(distRatio)
        let distRatioCosine = cos(distRatio)
        
        let startLatRad = PDR.degreesToRadians(startPoint.coordinate.latitude)
        let startLonRad = PDR.degreesToRadians(startPoint.coordinate.longitude)
        
        let startLatCos = cos(startLatRad)
        let startLatSin = sin(startLatRad)
        
        let endLatRads = asin((startLatSin * distRatioCosine) + (startLatCos * distRatioSine * cos(initialBearingRadians)))
    
        let endLonRads = startLonRad + atan2((sin(initialBearingRadians) * distRatioSine * startLatCos), distRatioCosine - startLatSin * sin(endLatRads))
        
        return CLLocation(latitude: PDR.radiansToDegrees(endLatRads), longitude: PDR.radiansToDegrees(endLonRads))
    }
    
    class func degreesToRadians(degrees : Double) -> Double
    {
        let degToRadFactor : Double = M_PI / 180
        return degrees * degToRadFactor
    }
    
    class func radiansToDegrees(radians : Double) -> Double
    {
        let radToDegFactor = 180 / M_PI
        return radians * radToDegFactor
    }
}