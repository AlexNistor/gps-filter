//
//  ViewController.swift
//  gps
//
//  Created by Alexandru Nistor on 01/07/16.
//  Copyright © 2016 Alexandru Nistor. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController{
    
    @IBOutlet weak var mapVIew: MKMapView!
    
    var locationManager = CLLocationManager()
    var canRegisterLocation = false
    var locations : [CLLocation] = []
    var locationsUntouched : [CLLocation] = []
    var cameraOnce : Bool = false
    
    var currentHeading : Double = 0.0
    
    var fileIndex = 0;
    
    var nString : String = ""
    
    var timer : NSTimer!
    var first : Bool = true
    
    private var gpsFilter = GPS_Filter()
    private var kalmanFilter = KalmanFilter()
    
    private var currentLocation : CLLocation!
    private var circ : MKCircle!
    
    var myPin : MKPointAnnotation!
    private var region : MKCoordinateRegion!
    
    private var showOtherColor = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        
        self.mapVIew.delegate = self
        
        FileManager.sharedInstance.readFromFile()
        
        self.addAllPointsOnMap(FileManager.sharedInstance.dummyLocations)
        
//        self.showPointsCorrectedByFilter()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //register location
    @IBAction func startAction(sender: AnyObject) {
        timer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(ViewController.updateLocationFromFile), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    @IBAction func stopAction(sender: AnyObject) {
        self.timer.invalidate()
    }
    
    @objc func updateLoc(){
        if canRegisterLocation {
            self.locationManager.startUpdatingLocation()
            self.locationManager.startUpdatingHeading()
        }else{
            self.locationManager.stopUpdatingLocation()
            self.locationManager.stopUpdatingHeading()
        }
    }
    
    @IBAction func printLog(sender: UIButton) {
        let areaWithCorrections = self.calcArea(self.locations)
        let areaWithOutCorrections = self.calcArea(self.locationsUntouched)
        nString += "AREA WITH CORRECTIONS IS:  \(areaWithCorrections)\n"
        
        fileIndex += 1
        self.nString = ""

        for loc in self.locationsUntouched {
            self.nString += "\(loc.coordinate.latitude),\(loc.coordinate.longitude)\n"
        }
        
        nString += "AREA WITHOUT CORRECTIONS IS:  \(areaWithOutCorrections)\n"
        nString += "Difference is: \(areaWithOutCorrections - areaWithCorrections)"
        
        nString = ""
        fileIndex += 1
        self.locations = []
        self.locationsUntouched = []
    }
    
    private func calcArea(locs : [CLLocation]) -> Double {
        var aria : Double = 0.0
        
        for i in 0 ..< locs.count {
            let currentLoc = locs[i].coordinate
            aria += currentLoc.latitude * currentLoc.longitude + 1 - currentLoc.latitude + 1 * currentLoc.longitude
        }
        
        return aria
    }
    
    private func addAnnotation(loc : CLLocationCoordinate2D){
        if !cameraOnce {
            let region = MKCoordinateRegionMakeWithDistance(loc, 100, 100);
            self.mapVIew.setRegion(region, animated: true)
            cameraOnce = true
        }
        
        if !self.mapVIew.annotations.isEmpty {
            self.mapVIew.removeAnnotations(self.mapVIew.annotations)
        }
        
        // Add an annotation
        let point = MKPointAnnotation()
        point.coordinate = loc
        self.mapVIew.addAnnotation(point)
    }
    
    private func addAllPointsOnMap(array : [CLLocation]){
        var point : [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for p in array {
            point.append(p.coordinate)
        }
        
        let polyLine = MKPolyline(coordinates: &point, count: point.count)
        
        self.mapVIew.addOverlay(polyLine)
    }
    
    private func showPointsCorrectedByFilter(){
        var newPoints : [CLLocation] = []
        
        for loc in FileManager.sharedInstance.dummyLocations {
            let (newLocation, shouldUpdate) = self.gpsFilter.statisticalCorrectionFilter(loc)
            
            if shouldUpdate {
                self.currentLocation = newLocation
                newPoints.append(newLocation)
            }
        }
        
        showOtherColor = true
        self.addAllPointsOnMap(newPoints)
    }
}

extension ViewController : CLLocationManagerDelegate, MKMapViewDelegate {
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
//        self.currentHeading = newHeading.trueHeading
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last!

        var (newLocation, shouldUpdate) = self.gpsFilter.statisticalCorrectionFilter(location)
        
        if shouldUpdate {
            self.currentLocation = newLocation
        }else {
            newLocation = self.currentLocation
        }
        if newLocation.coordinate.latitude == 0.0 &&  newLocation.coordinate.longitude == 0.0 {
            newLocation = location
            return
        }
        
        if first {
            self.region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 800, 800)
            self.mapVIew.setRegion(region, animated: true)
            self.first = false
        }
        
//        self.showCircle(newLocation)
        self.showMyPoint(newLocation)
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.stopUpdatingHeading()
    }
    
    @objc func updateLocationFromFile(){
        if FileManager.sharedInstance.locationIndex >= FileManager.sharedInstance.dummyLocations.count {
            self.timer.invalidate()
            print("DONE UPDATING LOCATION")
            return
        }
        
        let location = FileManager.sharedInstance.dummyLocations[FileManager.sharedInstance.locationIndex]
//        
//        var (newLocation, shouldUpdate) = self.gpsFilter.statisticalCorrectionFilter(location)
//        
//        if shouldUpdate {
//            self.currentLocation = newLocation
//        }else {
//            newLocation = self.currentLocation
//        }
//        if newLocation.coordinate.latitude == 0.0 &&  newLocation.coordinate.longitude == 0.0 {
//            newLocation = location
//            return
//        }
        
        if first {
            self.region = MKCoordinateRegionMakeWithDistance(location.coordinate, 800, 800)
            self.mapVIew.setRegion(region, animated: true)
            self.first = false
        }
        
//        self.showCircle(newLocation)
        self.showMyPoint(location)
        
        FileManager.sharedInstance.locationIndex += 1
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = !showOtherColor ? UIColor.blueColor() : UIColor.redColor()
            polylineRenderer.lineWidth = 2
            return polylineRenderer
        }
        
//        let circleRenderer = MKCircleRenderer(overlay: overlay)
//        
//        circleRenderer.fillColor = UIColor.blueColor()
//        circleRenderer.alpha = 0.5
        
        return MKOverlayRenderer()
    }
    
    func showCircle(loc : CLLocation){
        if self.mapVIew != nil && gpsFilter.averageDistance != 0.0{
            self.circ = MKCircle(centerCoordinate: loc.coordinate, radius: gpsFilter.averageDistance)
            
            let allOverlays = self.mapVIew.overlays
            
            if !allOverlays.isEmpty {
                self.mapVIew.removeOverlays(allOverlays)
            }
            
            self.mapVIew.addOverlay(circ)
            
//           self.Floor.text! = "D: \(String(format: "%.1f", gpsFilter.distance)) M: \(String(format: "%.1f", gpsFilter.averageDistance))  Aq: \(String(format: "%.1f", loc.horizontalAccuracy))  AvgThresh: \(gpsFilter.averageThreshold)"
        }
    }
    
    func showMyPoint(loc : CLLocation){
        if myPin == nil {
            myPin = MKPointAnnotation()
        }
        
        myPin.coordinate = loc.coordinate
        
        if self.mapVIew.annotations.isEmpty {
            self.mapVIew.addAnnotation(self.myPin)
        }
    }
}

