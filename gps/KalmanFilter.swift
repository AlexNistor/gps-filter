//
//  KalmanFilter.swift
//  bgs-ios
//
//  Created by Alexandru Nistor on 14/07/16.
//  Copyright © 2016 ASSIST Software. All rights reserved.
//

import MapKit

class KalmanFilter {
    private var i = 0
    
    // GLOBALS
    private var step = 0
    
    private var X_k_init : [[Double]] = [[0.0], [0.0], [0.0], [0.0]] //Location only for first value
    
    private var X_k0 : [[Double]] = [[0], [0], [0], [0]] //Using X_k_init we can compute velocity for initial condit[ion (wich will be previous state)
    private var X_kp : [[Double]] = [[0], [0], [0], [0]] //New state
    var X_k1 : [[Double]] = [[0], [0], [0], [0]] //New state
    private var Y_k : [[Double]] = [[0], [0], [0], [0]]
    
    // [ [Ox*Ox, Ox*Oy, Ox*Ovx, Ox*Ovy], [Oy*Ox, Oy*Oy, Oy*Ovx, Oy*Ovy], [Ovx*Ox, Ovx*Oy, Ovx*Ovx, Ovx*Ovy], [Ovy*Ox, Ovy*Oy, Ovy*Ovx, Ovy*Ovy] ]
    private var P_cm0 : [[Double]] = [[0.0000036,0,0,0],[0,0.0000036,0,0], [0,0,0.000001,0], [0,0,0,0.000001]] //Initial Process covariance matr
    private var observationError : [[Double]] = [[0.00000361,0,0,0],[0,0.00000361,0,0], [0,0,0.0000011,0], [0,0,0,0.0000011]]
    private var P_cmp : [[Double]] = []
    private var I_m : [[Double]] = [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]]
    private var KG : [[Double]] = []
    
    private var Ac : [[Double]] = [[0.00000001],[0.00000001]]
    
    // var a_k1 = [[0, 0]];
    private var interval_s : Double = 0.5
    private var kgv_var = 1.5
    
    //Matrix conversion vectors
    private var A : [[Double]] = []
    private var B : [[Double]] = []
    private var KGv : [[Double]] = []
    private var last_Location : CLLocation!
    private var timer : NSTimer?
    
    init(){
        A = [[1, 0, interval_s, 0], [0, 1, 0, interval_s], [0, 0, 1, 0], [0, 0, 0, 1]]
        B = [[0.5 * interval_s * interval_s, 0], [0, 0.5 * interval_s * interval_s], [interval_s, 0], [0, interval_s]]
    }
    
    func getLocationsNeccessary(location : CLLocation) {
        if step == 0 {
            self.X_k_init = [[location.coordinate.latitude], [location.coordinate.longitude], [0], [0]]
        } else if step == 1 {
            self.X_k0 = [
                [location.coordinate.latitude],
                [location.coordinate.longitude],
                [(location.coordinate.latitude - X_k_init[0][0]) / interval_s],
                [(location.coordinate.longitude - X_k_init[1][0]) / interval_s]
            ]
            
        } else {
            self.last_Location = location
            // set timer for next step
            if self.timer == nil {
                self.timer = NSTimer.scheduledTimerWithTimeInterval(interval_s, target: self, selector: #selector(KalmanFilter.next_step), userInfo: nil, repeats: true)
            }
            
        }
        step += 1
    }
    
    @objc func next_step() {
        //Compute predicted state
        X_kp = m_(A, b: X_k0)
    
        Y_k = [[last_Location.coordinate.latitude], [last_Location.coordinate.longitude], [(last_Location.coordinate.latitude - X_k0[0][0]) / 0.9], [(last_Location.coordinate.longitude - X_k0[1][0]) / 0.9]]
        
        //Compute predicted covariance matrix
        P_cmp = clean_m(m_(m_(A, b: P_cm0),b: m_t(A)))

        //Compute Kalman Gain
        KGv = [ [kgv_var, 0, 0, 0], [0, kgv_var, 0, 0], [0, 0, kgv_var, 0], [0, 0, 0, kgv_var]]

        P_cmp = m_(KGv, b: P_cmp)
        KG = d_(P_cmp,b: a_(P_cmp, b: observationError))

        //Compute current position

        X_k1 = a_(X_kp,b: m_(KG,b: s_(Y_k, b: X_kp)))
        
        let filteredLoc = CLLocation(latitude: self.X_k1[0][0], longitude: self.X_k1[1][0])
        let controller = (UIApplication.sharedApplication().keyWindow!.rootViewController) as! ViewController
        
        controller.showMyPoint(filteredLoc)
        print(filteredLoc.coordinate)
        //prepare next step
        X_k0 = X_k1;
        P_cm0 = m_(s_(I_m, b: KG),b: P_cmp)
    }
    
    // MARK: - Private Methods
    private func s_(a : [[Double]], b : [[Double]]) -> [[Double]] {
        let aNumRows = a.count, aNumCols = a[0].count
        
        let col = Array(count: aNumCols, repeatedValue: 0.0)
        var m = Array(count: aNumRows, repeatedValue: col)
        
        for r in 0 ..< aNumRows {
            // initialize the current row
            for c in 0 ..< aNumCols {
                m[r][c] = a[r][c] - b[r][c]
            }
        }
        return m
    }
    
    private func m_ (a : [[Double]], b : [[Double]]) -> [[Double]]{
        
        let aNumRows = a.count, aNumCols = a[0].count, bNumCols = b[0].count
        
        let col = Array(count: bNumCols, repeatedValue: 0.0)
        var m = Array(count: aNumRows, repeatedValue: col)

        for r in 0 ..< aNumRows {
            for c in 0 ..< bNumCols {
                m[r][c] = 0;             // initialize the current cell
                for i in 0 ..< aNumCols {
                    m[r][c] += a[r][i] * b[i][c];
                }
            }
        }
        return m
    }
    
    private func m_t(a : [[Double]]) -> [[Double]]{
    
        let aNumRows = a.count
        let aNumCols = a[0].count
        
        let col = Array(count: aNumCols, repeatedValue: 0.0)
        var m = Array(count: aNumRows, repeatedValue: col)
        
        for i in 0 ..< aNumRows {
            for j in 0 ... i {
                m[i][j] = a[j][i]
                m[j][i] = a[i][j]
            }
        }
        return m
    }
    
    private func d_(a : [[Double]], b : [[Double]]) -> [[Double]] {
        
        let aNumRows = a.count, aNumCols = a[0].count  // initialize array of rows
        
        let col = Array(count: aNumCols, repeatedValue: 0.0)
        var m = Array(count: aNumRows, repeatedValue: col)
        
        for r in 0 ..< aNumRows {
            // initialize the current row
            for c in 0 ..< aNumCols {
                if(b[r][c] != 0){
                    m[r][c] = a[r][c] / b[r][c]
                }else{
                    m[r][c] = 0
                }
            }
        }
        return m
    }
    
    private func a_(a : [[Double]], b : [[Double]]) -> [[Double]] {
        let aNumRows = a.count, aNumCols = a[0].count
        
        let col = Array(count: aNumCols, repeatedValue: 0.0)
        var m = Array(count: aNumRows, repeatedValue: col)
        
        for r in 0 ..< aNumRows {
            for c in 0 ..< aNumCols {
                m[r][c] = a[r][c] + b[r][c]
            }
        }
        return m
    }
    
    private func clean_m(a : [[Double]]) -> [[Double]]{
        var b = a
        
        let aNumRows = a.count, aNumCols = a[0].count
        for r in 0 ..< aNumRows {
            for c in 0 ..< aNumCols {
                if r != c {
                    b[r][c] = 0
                }
            }
        }
            
        return b
    }
}
